<?php declare(strict_types=1);

namespace App;

class App
{
    /** @var int */
    protected $room;

    public function __construct()
    {
        $this->room = 1;
    }

    /**
     * @return int
     */
    public function getRoom(): int
    {
        return $this->room;
    }
//
//    public function getRoom2($model): int
//    {
//        return 30;
//    }
//
//    public function getRoom3($model): int
//    {
//        return $this->getRoom2() * 3;
//    }
//
//    public function getRoom4($model): int
//    {
//        return $this->getRoom2() * 3;
//    }

}